﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sun : MonoBehaviour
{
    public float speed;
    public Transform myTransform;
    public float scaling;

    private float startScale;

    // Start is called before the first frame update
    void Start()
    {
        myTransform = gameObject.transform;
        startScale = myTransform.localScale.x;
    }

    // Update is called once per frame
    void Update()
    {
        myTransform.Rotate(0, 0, speed, Space.World);

        float newScale = startScale + scaling * Mathf.Sin(Time.time);
        myTransform.localScale = new Vector3(newScale, newScale, newScale);
    }
}
