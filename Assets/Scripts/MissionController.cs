﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class MissionController : MonoBehaviour
{
    public Flowchart flowchartMain;
    public Flowchart flowchartBun;
    public Flowchart flowchartSwan;

    public GameObject shelterArea;
    public GameObject forest1Area;
    public GameObject forest2Area;

    public GameObject inventory;

    public bool swanMissionComplete = false;
    public bool bunMissionComplete = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartBunMission()
    {
        //When bun mission is ongoing, swan egg is disabled, swan and caretaker will say different things, and bun parents are enabled
        Debug.Log("bunmission started");
        forest2Area.transform.Find("BunparentsScene").gameObject.SetActive(true);
    }

    public void EndBunMission()
    {
        //When bun mission has ended, enable swan parent at shelter, empty bag
        Debug.Log("bunmission finished");
        
        //enable swan parent
    }
}
