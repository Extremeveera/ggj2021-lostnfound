﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class Inventory : MonoBehaviour
{
    public GameObject inventory;
    public GameObject shelterOutside;
    public GameObject forest1;

    public Flowchart flowchartBun;

    public bool invBabyBun = false; //Baby bun is in the bag
    public bool invSwanEgg = false;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BabyBunToBag()
    {
        //when baby bun is in the bag, activate mission, disable baby bun overworld object and enable baby bun bag sprite

        invBabyBun = true;

        Debug.Log("bun in bag");
        shelterOutside.transform.Find("Bun-child").gameObject.SetActive(false);
        inventory.transform.Find("bunInBag").gameObject.SetActive(true);

        flowchartBun.ExecuteBlock("Mission1_Start");
    }

    public void BunOutOfBag()
    {
        inventory.transform.Find("bunInBag").gameObject.SetActive(false);
    }

    public void SwanEggToBag()
    {
        invSwanEgg = true;

        Debug.Log("egg in bag");
        forest1.transform.Find("Swan-egg").gameObject.SetActive(false);
        inventory.transform.Find("eggInBag").gameObject.SetActive(true);

        //flowchartBun.ExecuteBlock("Mission1_Start");
    }
    public void EggOutOfBag()
    {
        Debug.Log("egg out");
        inventory.transform.Find("eggInBag").gameObject.SetActive(false);
    }
}
