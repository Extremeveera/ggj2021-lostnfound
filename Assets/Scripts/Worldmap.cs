﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Worldmap : MonoBehaviour
{
    public static bool MapIsOpen = false;
    public GameObject pauseMenuUI;
    public GameObject shelterArea;
    public GameObject forest1Area;
    public GameObject forest2Area;
    public GameObject hole;
    public GameObject end;
    public GameObject mapButton;

    //public GameObject overworldkids;
    public GameObject missionController;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void BackToGame()
    {
        pauseMenuUI.SetActive(false);
    }

    public void OpenMap()
    {
        pauseMenuUI.SetActive(true);
    }

    public void MoveToShelter()
    {
        shelterArea.SetActive(true);
        forest1Area.SetActive(false);
        forest2Area.SetActive(false);
        hole.SetActive(false);
    }

    public void MoveToForest1()
    {
        shelterArea.SetActive(false);
        forest1Area.SetActive(true);
        forest2Area.SetActive(false);
        hole.SetActive(false);

        //overworldkids.transform.Find("Bun-child").gameObject.SetActive(false);
    }
    public void MoveToForest2()
    {
        shelterArea.SetActive(false);
        forest1Area.SetActive(false);
        forest2Area.SetActive(true);
        hole.SetActive(false);

        //overworldkids.transform.Find("Bun-child").gameObject.SetActive(false);
    }

    public void MoveToEnd()
    {
        shelterArea.SetActive(false);
        forest1Area.SetActive(false);
        forest2Area.SetActive(false);
        hole.SetActive(false);
        end.SetActive(true);
    }

    public void EnableSwanMission()
    {
        Debug.Log("swan appeared");
        forest1Area.transform.Find("Swan-inscene1").gameObject.SetActive(false);
        shelterArea.transform.Find("Swan-inscene2").gameObject.SetActive(true);
    }

    public void EggFalls()
    {
        forest1Area.transform.Find("Swan-egg").gameObject.SetActive(true);
    }

    public void EnableBearMission()
    {
        shelterArea.transform.Find("Swan-inscene2").gameObject.SetActive(false);
    }

    public void MoveToHole()
    {
        shelterArea.SetActive(false);
        forest1Area.SetActive(false);
        forest2Area.SetActive(false);
        hole.SetActive(true);

        mapButton.SetActive(false);

        //overworldkids.transform.Find("Bun-child").gameObject.SetActive(false);
    }
}
